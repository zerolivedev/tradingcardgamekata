FROM node:latest

WORKDIR /opt/kata

COPY package* ./

RUN npm install

COPY ./ ./
