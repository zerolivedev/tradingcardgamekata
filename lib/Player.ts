import { Deck } from "./Player/Deck"
import { Hand } from "./Player/Hand"
import { Card } from "./Player/Card"


export class Player {
  public static start():Player {
    const deck:Deck = Deck.complete()

    return new Player(deck)
  }

  private BLEEDING_OUT:number = 1
  private MAXIMUM_MANA_SLOTS:number = 10
  private FULL_LIFE:number = 30
  private currentLifePoints:number = this.FULL_LIFE
  private currentManaSlots:number = 0
  private currentFilledManaSlots:number = 0
  private deck:Deck
  private isFirstActivation:boolean = true
  private hand:Hand = new Hand()

  constructor(deck:Deck) {
    this.deck = deck
  }

  public takeDamage(damagePoints:number):void {
    this.currentLifePoints -= damagePoints
  }

  public active():void {
    if (!this.isFullOfManaSlots()) {
      this.retrieveManaSlot()
      this.fillManaSlots()
    }

    this.drawDeckCards()

    this.isFirstActivation = false
  }

  public play():Card {
    const card:Card = this.deck.draw()

    this.spendFilledManaSlots(card.manaCost())

    return card
  }

  public lifePoints():number {
    return this.currentLifePoints
  }

  public manaSlots():number {
    return this.currentManaSlots
  }

  public filledManaSlots():number {
    return this.currentFilledManaSlots
  }

  public deckCards():number {
    return this.deck.quantityOfCards()
  }

  public handCards():number {
    return this.hand.count()
  }

  private fillManaSlots():void {
    this.currentFilledManaSlots = this.currentManaSlots
  }

  private spendFilledManaSlots(quantity:number):void {
    this.currentFilledManaSlots -= quantity
  }

  private retrieveManaSlot():void {
    this.currentManaSlots += 1
  }

  private drawDeckCards():void {
    if(!this.deck.isEmpty()) {
      if(this.hasToDrawInitialHand()) {
        this.drawInitialHand()
      }

      this.drawCards()
    } else {
      this.takeBleedOutDamage()
    }
  }

  private takeBleedOutDamage():void {
    this.takeDamage(this.BLEEDING_OUT)
  }

  private drawCards():void {
    this.deck.draw()
    this.hand.draw()
  }

  private drawInitialHand():void {
    this.drawCards()
    this.drawCards()
    this.drawCards()
  }

  private isFullOfManaSlots():boolean {
    return (this.currentManaSlots >= this.MAXIMUM_MANA_SLOTS)
  }

  private hasToDrawInitialHand():boolean {
    return this.isFirstActivation
  }
}
