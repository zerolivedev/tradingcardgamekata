import { shuffle } from "../utils/shuffle"
import { Cards } from "./Deck/Cards"
import { Card } from "./Card"

export class Deck {
  public static with(manaCosts:Array<number>):Deck {
    return new Deck(manaCosts)
  }

  public static complete():Deck {
    const completedManaCosts:Array<number> = [0,0,1,1,2,2,2,3,3,3,3,4,4,4,5,5,6,6,7,8]

    return Deck.with(completedManaCosts)
  }

  private cards:Cards

  constructor(cards:Array<number>) {
    const randomOrderedManaCosts:Array<number> = this.shuffleManaCosts(cards)

    this.cards = new Cards(randomOrderedManaCosts)
  }

  public draw():Card {
    return this.cards.take()
  }

  public quantityOfCards():number {
    return this.cards.quantity()
  }

  public isEmpty():boolean {
    return (this.quantityOfCards() === 0)
  }

  private shuffleManaCosts(cards:Array<number>):Array<number> {
    return shuffle(cards)
  }
}
