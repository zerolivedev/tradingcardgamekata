import { Card } from "../Card"

export class Cards {
  private cards:Array<Card>

  constructor(manaCosts:Array<number>) {
    this.cards = manaCosts.map((manaCost) => {
      return new Card(manaCost)
    })
  }

  public take():Card {
    return this.cards.pop() || new Card(0)
  }

  public quantity():number {
    return this.cards.length
  }
}
