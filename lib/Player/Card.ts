
export class Card {
  private points:number

  constructor(manaCost:number) {
    this.points = manaCost
  }

  public manaCost():number {
    return this.points
  }

  public damage():number {
    return this.points
  }
}
