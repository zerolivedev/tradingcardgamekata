
export class Hand {
  private OVERLOAD_THRESHOLD:number = 5
  private quantity:number = 0

  public draw():void {
    this.quantity += 1

    if(this.isOverloaded()) {
      this.discard()
    }
  }

  public count():number {
    return this.quantity
  }

  private discard() {
    this.quantity -= 1
  }

  private isOverloaded():boolean {
    return (this.quantity > this.OVERLOAD_THRESHOLD)
  }
}
