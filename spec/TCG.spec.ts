import { expect } from "chai"
import { Player } from "../lib/Player"
import { Deck } from "../lib/Player/Deck"
import { Card } from "../lib/Player/Card"

describe("Trading Card Game", () => {
  describe("Player", () => {
    const fullLife:number = 30

    it("takes damage", () => {
      const player:Player = Player.start()
      const damagePoints:number = 2

      player.takeDamage(damagePoints)

      expect(player.lifePoints()).to.eq(fullLife - damagePoints)
    })

    it("discards a card when the hand is overload", () => {
      const player:Player = Player.start()

      activeMoreThanTenTimes(player)

      const quantityOfCardsForOverload:number = 5
      expect(player.handCards()).to.eq(quantityOfCardsForOverload)
    })

    it("plays a card that do a damage equal to its mana cost", () => {
      const noManaCost:number = 0
      const rawDeckWithoutDamage = [noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost,noManaCost]
      const deck:Deck = Deck.with(rawDeckWithoutDamage)
      const player:Player = new Player(deck)

      const card:Card = player.play()

      expect(card.damage()).to.eq(noManaCost)
    })

    it("consumes filled mana slots equal to card mana cost", () => {
      const oneManaCost:number = 1
      const rawDeckWithoutDamage = [oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost,oneManaCost]
      const deck:Deck = Deck.with(rawDeckWithoutDamage)
      const player:Player = new Player(deck)
      player.active()

      player.play()

      expect(player.filledManaSlots()).to.eq(0)
    })

    describe("on start", () => {
      it("is full life", () => {

        const player:Player = Player.start()

        expect(player.lifePoints()).to.eq(fullLife)
      })

      it("is without mana slots", () => {

        const player:Player = Player.start()

        expect(player.manaSlots()).to.eq(0)
      })

      it("is with a complete deck of cards", () => {

        const player:Player = Player.start()

        const quantityOfCardsForACompleteDeck:number = 20
        expect(player.deckCards()).to.eq(quantityOfCardsForACompleteDeck)
      })

      it("is without filled mana slots", () => {

        const player:Player = Player.start()

        expect(player.filledManaSlots()).to.eq(0)
      })
    })

    describe("on active", () => {
      it("retrieves a mana", () => {
        const player:Player = Player.start()

        player.active()

        expect(player.manaSlots()).to.eq(1)
      })

      it("fills mana slots", () => {
        const player:Player = Player.start()

        player.active()

        expect(player.filledManaSlots()).to.eq(1)
      })

      it("retrieves a mana slot until be full", () => {
        const player:Player = Player.start()

        activeMoreThanTenTimes(player)

        const maximumManaSlots:number = 10
        expect(player.manaSlots()).to.eq(maximumManaSlots)
      })

      it("per first time draws initial hand and a card from deck", () => {
        const player:Player = Player.start()

        player.active()

        expect(player.deckCards()).to.eq(16)
        expect(player.handCards()).to.eq(4)
      })

      it("after first time draws a card from deck", () => {
        const player:Player = Player.start()
        player.active()

        player.active()

        expect(player.deckCards()).to.eq(15)
        expect(player.handCards()).to.eq(5)
      })

      it("takes damage when there are no cards to draw", () => {
        const player:Player = Player.start()
        const bleedingOutDamage = 1

        activateUntilBleedOut(player)

        expect(player.deckCards()).to.eq(0)
        expect(player.lifePoints()).to.eq(fullLife - bleedingOutDamage)
      })
    })
  })

  function activeMoreThanTenTimes(player:Player):void {
    activateTenTimes(player)
    player.active()
  }

  function activateUntilBleedOut(player:Player):void {
    activateTenTimes(player)
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
  }

  function activateTenTimes(player:Player):void {
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
    player.active()
  }
})
