# Trading Card Game kata

## System requirements for run tests

- `Docker version 20.10.18` or compatible
- `docker-compose version v2.3.3` or compatible

## For run tests

First of all, you have to build the project with the command:

`docker-compose up --build`

Now, you can run tests with the following command:

`docker-compose run --rm kata npm test`

> Once you end to work with the project I recommend to you that stop the project by using: `docker-compose down`
